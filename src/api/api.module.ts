import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiController } from './api.controller';
import { ApiService } from './api.service';
import { ApiRepository } from './apiRepository';

@Module({
  controllers: [ApiController],
  providers: [ApiService],
  imports: [TypeOrmModule.forFeature([ApiRepository])],
})
export class ApiModule {}
  