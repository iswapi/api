import { ApiProperty } from "@nestjs/swagger";
import { IsString } from 'class-validator';

export class UpdateApi {
    @IsString()
    @ApiProperty({ type: String })
    uuid: string;

    @IsString()
    @ApiProperty({ type: String })
    ownerid: string;

    @IsString()
    @ApiProperty({ type: String })
    name: string;

    @IsString()
    @ApiProperty({ type: String })
    topic: string;

    @IsString()
    @ApiProperty({ type: String })
    description: string;

    @IsString()
    @ApiProperty({ type: String })
    baseurl: string;

    @IsString()
    @ApiProperty({ type: String })
    deployurl: string;
}

export class CreateApi{
    @IsString()
    @ApiProperty({ type: String })
    uuid: string;

    @IsString()
    @ApiProperty({ type: String })
    ownerid: string;

    @IsString()
    @ApiProperty({ type: String })
    name: string;

    @IsString()
    @ApiProperty({ type: String })
    description: string;

    @IsString()
    @ApiProperty({ type: String })
    topic: string;
}