import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import Api from './models/api.entity';
import { ApiRepository } from './apiRepository';

@Injectable()
export class ApiService {
  constructor(
    @InjectRepository(Api)
    private apiRepository: ApiRepository,
  ) {}


  welcome(): string {
    return "Welcome to the Api API";
  }

  async getApi(id: string): Promise<Api> {
    return await this.apiRepository.findOne(id);
  }

  async addApi(api: Api): Promise<Api> {
    return await this.apiRepository.save(api);
  }

  async delApi(id: string) {
    return await this.apiRepository.delete(id);
  }

  async updApi(api: Api): Promise<Api> {
    return await this.apiRepository.save(api);
  }
}
