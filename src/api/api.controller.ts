import { BadRequestException, Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import Api from './models/api.entity'
import { CreateApi, UpdateApi } from './models/api.dto';
import { ApiService } from './api.service';

@Controller('api')
export class ApiController {
  constructor(private readonly apiService: ApiService) {}

  @Get("/")
  baseGet(): string {
    return this.apiService.welcome();
  }

  @Get("/:id")
  async getApi(@Param('id') id): Promise<Api> {
    return await this.apiService.getApi(id);
  }

  @Post("/")
  async createApi(@Body() api: CreateApi): Promise<Api> {
    const u: Api = { ...api };
    return await this.apiService.addApi(u);
  }

  @Delete(":id")
  async deleteApi(@Param('id') id: string): Promise<string> {
    const api: Api = await this.apiService.getApi(id)
    if (!api) {
      throw new BadRequestException(
        `No api with this id :${id}`,
      );
    }
    this.apiService.delApi(id);  // better error handeling
    return "Api successfully deleted";
  }

  @Put("/")
  async updateApi(@Body() body: UpdateApi): Promise<string> {
    const newApi: Api = { ...body };

    if (!newApi) {
      throw new BadRequestException(
        //incorrect implementation of a api JSON
        `cannot update api`,
      );
    }

    await this.apiService.getApi(newApi.uuid);

    return "Api successfully updated"
  }
}
