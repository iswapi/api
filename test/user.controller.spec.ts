import { Test, TestingModule } from '@nestjs/testing';
import User from '../src/user/models/api.entity';
import { ApiRepository } from '../src/user/apiRepository';
import { ApiController } from '../src/user/api.controller';
import { ApiService } from '../src/user/api.service';

describe('UserController', () => {
  //create a Mock for the database
  const mockUser = new User();

  let mockUserService = {
    welcome: () => "Welcome to the User API",
    getUser: (id: string) => undefined,
    addUser: (user: User) => (!user ? undefined : mockUser),
    updUser: () => [mockUser],
    delUser: (id: string) => undefined
  };
  
  let userController: ApiController;
  let userService: ApiService;

  beforeEach(async () => {
    userService = new ApiService(new ApiRepository());
    userController = new ApiController(userService);

    const app: TestingModule = await Test.createTestingModule({
      controllers: [ApiController],
      providers: [ApiService],
    }).overrideProvider(ApiService)
      .useValue(mockUserService)
      .compile();

    userController = app.get<ApiController>(ApiController);
  });

  describe('root', () => {
    it('"', () => {
      expect(userController.baseGet()).toBe('Welcome to the User API');
    });
  });
});
