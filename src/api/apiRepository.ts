import Api from './models/api.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Api)
export class ApiRepository extends Repository<Api> {}