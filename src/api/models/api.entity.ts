import { Entity, Column, PrimaryColumn } from "typeorm";

@Entity()
export default class Api {
    constructor() {
        this.uuid = '';
        this.ownerid = '';
        this.name = '';
        this.topic = '';
        this.description = '';
        this.baseurl = '';
        this.deployurl = '';
    }

    @PrimaryColumn({
        length: 150
    })
    uuid!: string;

    @Column("text")
    ownerid!: string;

    @Column("text")
    name!: string;
    
    @Column("text")
    topic!: string;

    @Column("text")
    description?: string;

    @Column("text")
    baseurl?: string;

    @Column("text")
    deployurl?: string;
}