import { Module } from '@nestjs/common';
import { ApiModule } from './api/api.module'
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    ApiModule,
    TypeOrmModule.forRoot()
  ]
})

export class AppModule {}
